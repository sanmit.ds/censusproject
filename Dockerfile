# Set the base image
FROM centos/python-36-centos7

# File Author / Maintainer
LABEL vendor="Sanmit/Classfier" \
      version="1.0.0"

USER root

################## BEGIN INSTALLATION ######################

# Installing OS dependencies
RUN yum update -y

# Installing python dependencies
COPY ./requirements.txt /opt/
RUN pip3 install --no-cache-dir -r /opt/requirements.txt

#Creating Application Source Code Directory
RUN mkdir -p /opt/WalmartMLE
RUN mkdir -p /opt/applogs

# Copying src code to Container
COPY ./ /opt/WalmartMLE
COPY startup /opt/WalmartMLE
RUN chmod 755 /opt/WalmartMLE/startup

# Application Environment variables
ENV PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV PYTHONPATH=.:/usr/local/lib/python36.zip:/usr/local/lib/python3.6:/usr/local/lib/python3.6/lib-dynload:/usr/local/lib/python3.6/site-packages:/opt/WalmartMLE

# Set where log files will be written
ENV LOG_DIR=/tmp

# Application startup parameters for FLASK
ENV SERVICE_PORT=7010
ENV IS_THREADED=True
ENV IS_DEBUG_ENABLED=False

# Setting Home Directory for containers
WORKDIR /opt/WalmartMLE

##################### INSTALLATION END #####################

# Exposing Ports
EXPOSE 7010

# Running Python Application
CMD [ "/opt/WalmartMLE/startup" ]
