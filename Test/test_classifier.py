"""
Unit tests for the classification service
"""
import pytest
from scaling_dummies_mapping import min_max_scaling, map_dummies, min_max_mapping, get_min_max_scaling
from classifier_proc import classify_payload
from joblib import load
import traceback

class TestNameParserProc(object):
    test_case1 = min_max_mapping["age"]
    test_case1_result = 0.4444444444444444

    def test_min_max_scaling_1(self):
        test = min_max_scaling(40, self.test_case1["min_val"], self.test_case1["max_val"])

        assert test == self.test_case1_result

    test_case2 = ("race", "asian")
    test_case2_result = {'race_amer indian aleut or eskimo': 0, 'race_asian or pacific islander': 0, 'race_black': 0, 'race_other': 1, 'race_white': 0}

    def test_map_dummies_1(self):
        _cat_name = self.test_case2[0]
        _cat_val = self.test_case2[1]
        test = map_dummies(_cat_name, _cat_val)
        assert test == self.test_case2_result


    test_case3 = ("tax filer stat", "head of household")
    test_case3_result = {'tax filer stat_head of household': 1, 'tax filer stat_joint both 65+': 0, 'tax filer stat_joint both under 65': 0, 'tax filer stat_joint one under 65 & one 65+': 0, 'tax filer stat_nonfiler': 0, 'tax filer stat_single': 0}

    def test_map_dummies_2(self):
        _cat_name = self.test_case2[0]
        _cat_val = self.test_case2[1]
        test = map_dummies(_cat_name, _cat_val)
        assert test == self.test_case2_result

    test_case4 = (55, "wage per hour")
    test_case4_result = 0.005500550055005501

    def test_min_max_scaling_2(self):
        test = get_min_max_scaling(self.test_case4[0], self.test_case4[1])
        assert test == self.test_case4_result

    test_case5 = (55, "hands")
    test_case5_result = "unknown category hands"

    def test_min_max_scaling_3(self):
        try:
            test = get_min_max_scaling(self.test_case5[0], self.test_case5[1])
            assert False
        except:
            if self.test_case5_result in str(traceback.format_exc()):
                assert True
            else:
                assert False


    MODEL_PATH = '../models/Adaboost_2020-06-0811_22_49.284032.joblib'
    model = load(MODEL_PATH)

    test_case6 = {
    "country of birth father": "United-States",
    "detailed household and family stat": "Householder",
    "detailed household summary in household": "Householder",
    "detailed industry recode": 35,
    "education": "Prof school degree (MD DDS DVM LLB JD)",
    "full or part time employment stat": "Full-time schedules",
    "hispanic origin": "All other",
    "marital stat": "Married-civilian spouse present",
    "own business or self employed": 0,
    "race": "White",
    "sex": "Female",
    "tax filer stat": "Joint both under 65",
    "year": 95,
    "age": 58,
    "wage per hour": 0,
    "capital gains": 0,
    "capital losses": 0,
    "dividends from stocks": 0,
    "weeks worked in year": 52,
    "label": "50000+."
  }
    test_case6_result = '50000+'

    def test_classify_payload1(self):
        test = classify_payload(self.test_case6, self.model)
        assert test == self.test_case6_result

    test_case7 = {
    "country of birth father": "united-states",
    "detailed household and family stat": "other",
    "detailed household summary in household": "other relative of householder",
    "detailed industry recode": "0",
    "education": "HS_grad",
    "full or part time employment stat": "Not in labor force",
    "hispanic origin": "all other",
    "marital stat": "widowed",
    "own business or self employed": "0",
    "race": "white",
    "sex": "Female",
    "tax filer stat": "nonfiler",
    "year": "95",
    "age": 73,
    "wage per hour": 0,
    "capital gains": 0,
    "capital losses": 0,
    "dividends from stocks": 0,
    "weeks worked in year": 0
    }
    test_case7_result = "-50000"

    def test_classify_payload2(self):
        test = classify_payload(self.test_case6, self.model)
        assert test == self.test_case6_result
