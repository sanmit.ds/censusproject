# Cencus Data Classification
Goal of this project is to demonstrate a pipeline to
1. Fit/Refit model/models.
2. Deploy the model using Docker and have a scoring pipeline.

###Note: Objective of this project is not to display the accuracy of the model, it is to demonstrate the pipeline.

## Project Dependencies
* cencusproject (must be in PYTHONPATH)

## Summary
classifier_svc exposes a combination of RESTful request/response and machine learning services. 
Given census data classifier_svc returns whether the entry is in "50000+" income group or  "-50000" income group.
The currently release exposes the following components;

The Project contains a Jupyter Notebook to fit/refit a model and a REST service to deploy the service and access the model.

There are three parts to this:

1. Jupyter Notebooks
* ModelFitting.ipynb : Pipeline to fit/refit the model

2. API
* RESTful Web Service
* /api/check/v1 : provides up-time of the service since run [GET | POST]
* /api/classifier/v1 : currently supports classification between "50000+" income group or  "-50000" income group [POST]
* For local deployment, environment variables are unchanged then the IP address/Port will be 127.0.0.1:7010 
* Schema validation for input data


3. Python Scripts
* Shared classes used throughout the services
* classifier_svc.py : Provides classification service functionality (Provides access to the model trained in the Jupyter Notebook metioned above).
* classifier_proc.py : Functionality related to mapping input the the ML model and gathering output from the ML model
* scaling_dummies_mapping.py : Contains functionality to convert input into dummy variables or min max scaling
* utils.py : Utility functions to support the main functionality.
* Test/test_classifier.py : Has unit tests for various parts functions.
* Test/ClassifierTestPlan.jmx : Load test package to work with JMeter



## Required Packages/Modules
* Please refer to the requirements.txt
    * You might encounter an error installing uwsgi on Windows.
    You can remove it from the requirements.txt for windows platforms
* Jupyter notebook requires the following modules in addition to the ones mentioned in requirements.txt
    * pandas
    * numpy
    * matplotlib
    * seaborn
    * sklearn
    * joblib
    * Note: This note book was developed on an Aanaconda environment. For best results we recommend using conda 4.8.2 and Jupyter 6.0.3 
* Docker installation is a must for running the docker image

## Testing
To run pytest cases your environment needs to be configured with the packages defined in Required section plus the addition of pytest.
* Please run test_classifier.py under Test folder for unit tests
To tun the load test
* You will require jemeter installed.
* Then load the ClassifierTestPlan.jmx from the Test folder in JMeter and run the test.
    * Load test results are under load_test_result.csv

## REST service Local Dev Setup
* Copy cencusproject folder
* Add cencusproject to your system variables as PYTHONPATH
* Environment variables in the file env.list must be set on your system 
* Restart your IDE and make sure to run it with administrative access
* Add python 3 as your interpreter for your machine
* Run 'pip3 install --no-cache-dir -r requirements.txt' from cencusproject folder
    * Note: If you are on windows you might have problems installing uwsgi package. Please remove it from the requirements.txt file to get around that.
* Run the classifier_svc.py
* To run the Jupyter notebook

## REST service Docker Setup
* Make sure you are in the same directory as the Dockerfile
*  Build command 'docker build -t classifier-svc .'
*  Run command 'docker run -p 7010:7010 --env-file env.list classifier-sv'