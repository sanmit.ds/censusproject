"""
Utility functions to support the main functionality
"""

import platform
import logging
import logging.config
from os import getenv, path
import json


def is_none_or_empty_string(arg: str):
    """
    Checks to see if the argument is None or an empty string
    :param arg : string to be checked for None or Empty
    :returns (bool): True if None or Empty '' otherwise False
    """
    if arg is None:
        return True
    elif arg == '' or arg == '""' or arg == "''":
        return True
    elif arg == 'None':
        return True
    return False


def load_file(file_path, file_type='json'):
    """
    Read a file into memory, and parse based on fileType
    :param file_path : a string that points to a valid path + filename
    :param file_type : try to convert loaded file based on file type, i.e. return text or python dict
    :returns: dict object or string
    :raises: ValueError, TypeError, I/O errors
    """

    # Valiate intput arguments
    if is_none_or_empty_string(file_path):
        raise ValueError('filePath evaluated to None or Empty String')
    if file_type not in ('json', 'txt', 'text'):
        raise ValueError('Invalid parseAs {}'.format(file_type))

    with open(file_path) as rawFile:
        if file_type == 'json':
            return json.loads(rawFile.read())
        if file_type in ('txt', 'text'):
            return rawFile.read()
        return ''


def get_logger(filepath='', logger='default', useTemp=True, logLevel='error'):
    """convience function to  get a logger

    :param filepath : location of configuration file for logging - must be JSON format
    :param logger : name of logger you want to use inside configuration
    :param useTemp : if set to true, the full filepath is prefixed with the environment variable LOG_DIR location + base filename
    :returns (logging.logger): reference to a Logger instance

    :raises: ValueError
    """
    if is_none_or_empty_string(filepath):
        raise ValueError('filepath is None or empty, required')
    cfgDict = load_file(filepath)
    if useTemp:
        if any(platform.win32_ver()):
            cfgDict['handlers']['file']['filename'] = path.join(getenv('LOG_DIR', default=getenv('TEMP')), cfgDict['handlers']['file']['filename'])
        else:
            cfgDict['handlers']['file']['filename'] = path.join(getenv('LOG_DIR', default='/tmp'), cfgDict['handlers']['file']['filename'])
    logging.config.dictConfig(cfgDict)

    _log = logging.getLogger(logger)

    if logLevel in ('DEBUG', 'debug'):
        _log.setLevel(logging.DEBUG)
    elif logLevel in ('INFO', 'info'):
        _log.setLevel(logging.INFO)
    elif logLevel in ('WARNING', 'warning', 'WARN', 'warn'):
        _log.setLevel(logging.WARNING)
    elif logLevel in ('ERROR', 'error'):
        _log.setLevel(logging.ERROR)
    elif logLevel in ('FATAL', 'fatal', 'CRITICAL', 'critical'):
        _log.setLevel(logging.CRITICAL)
    else:
        _log.setLevel(logging.WARNING)

    return _log
