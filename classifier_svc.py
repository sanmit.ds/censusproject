"""
Provides classification service functionality.
"""

# Standard Library Packages
import os
import sys
import traceback
import logging
import time
import json


# Third Party Packages
from flask import Flask, request, Response
import jsonschema
import pandas as pd
from joblib import load

# Local App & Library Packages
from utils import get_logger, load_file
from classifier_proc import classify_payload

# Setup Logging (python logging)
_log = None
try:
    _log = get_logger(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'logging.json'))
    _log.debug(type(_log))

    if os.getenv('LOG_LEVEL') in ('DEBUG', 'debug'):
        _log.setLevel(logging.DEBUG)
    elif os.getenv('LOG_LEVEL') in ('INFO', 'info'):
        _log.setLevel(logging.INFO)
    elif os.getenv('LOG_LEVEL') in ('WARNING', 'warning', 'WARN', 'warn'):
        _log.setLevel(logging.WARNING)
    elif os.getenv('LOG_LEVEL') in ('ERROR', 'error'):
        _log.setLevel(logging.ERROR)
    elif os.getenv('LOG_LEVEL') in ('FATAL', 'fatal', 'CRITICAL', 'critical'):
        _log.setLevel(logging.CRITICAL)

except TypeError as e:
    print(traceback.format_exc())
    sys.exit(1)


# Contact the provision app to get a configuration
app_config = {}
try:
    _log.info('Get application configurations...')
    app_config['app_server'] = {}
    app_config['app_server']['host'] = os.getenv('SERVICE_HOST')
    app_config['app_server']['port'] = os.getenv('SERVICE_PORT')
    app_config['app_server']['threaded'] = os.getenv('IS_THREADED')
    app_config['app_server']['debug'] = os.getenv('IS_DEBUG_ENABLED')

except TypeError as e:
    _log.fatal(traceback.format_exc())
    sys.exit(1)

appStart = time.time()
app = Flask(__name__)

# load model
try:
    MODEL_PATH = os.getenv('MODEL_NAME')
    model = load(MODEL_PATH)
except FileNotFoundError as e:
    _log.fatal(traceback.format_exc())
    sys.exit(1)


@app.route('/api/check/v1', methods=['GET', 'POST'])
def check():
    """
    Provides end point to check if service is running and for how long in seconds it's been running.
    """
    _log.debug('check()')
    app_resp = {'uptime': str(int((time.time() - appStart) / 60)) + " mins"}
    return Response(json.dumps(app_resp), status=200, mimetype='application/json')


input_schema_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "input_schema.json")
classifier_schema = load_file(input_schema_file_path)


@app.route('/api/classifier/v1', methods=['POST'])
def classifier_api_v1():
    """
    Provides functionality for finding locations using v1 schema
    """
    _log.debug('classifier_api_v1()')
    response_payload = {"output": "No input available", "error": []}
    try:
        validation = jsonschema.Draft7Validator(classifier_schema).is_valid(request.json)
        if validation is True:
            request_payload = request.json
            # correlation_id = request_payload["correlation_id"]
            response_payload["output"] = classify_payload(request_payload, model)
            return Response(json.dumps(response_payload), status=200, mimetype='application/json')
        else:
            raise Exception("In valid JSON input")
    except:
        err_msg = traceback.format_exc()
        response_payload["error"].append(err_msg)
        _log.error(err_msg)

        return Response(json.dumps(response_payload), status=400, mimetype='application/json')


if __name__ == '__main__':
    _log.info('Starting Classifier Service')

    debugFlag = True if app_config['app_server']['debug'] == 'True' else False
    threadedFlag = True if app_config['app_server']['threaded'] == 'True' else False
    app.run(host=app_config['app_server']['host'],
            port=int(app_config['app_server']['port']),
            threaded=app_config['app_server']['threaded'],
            debug=app_config['app_server']['debug'])
