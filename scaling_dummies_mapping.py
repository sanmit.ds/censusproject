"""
Contains functionality to convert input into dummy variables or min max scaling
"""
import os
import copy
from utils import load_file

# mapping for min max scaling
min_max_mapping = load_file(os.getenv('MIN_MAX_MAPPING_FILE'))

# mapping for dummy variables
dummies_mapping = load_file(os.getenv('DUMMIES_FILE'))

keep_columns = set(list(dummies_mapping.keys()) + list(min_max_mapping.keys()))


def min_max_scaling(input_var, min_val, max_val):
    """
    Scale input by doing min max scaling
    if input_var less than min_val then min_val assumed
    if input_var greater than max_val then max_val assumed
    """
    if input_var < min_val:
        input_var = min_val

    elif input_var > max_val:
        input_var = max_val

    input_std = (input_var - min_val) / (max_val - min_val)
    return input_std


def map_dummies(cat_name, cat_val):
    """
    given a category value and category name, returns the dummy variales
    """
    _dummy_name = None
    if cat_name in dummies_mapping:
        _cat_dict = copy.deepcopy(dummies_mapping[cat_name])
        if cat_val in _cat_dict['lookup']:
            _dummy_name = _cat_dict['lookup'][cat_val]
        else:
            if 'other' in _cat_dict['lookup']:
                _dummy_name = _cat_dict['lookup']['other']
            else:
                raise Exception('Unmapped Value {} in category {}'.format(cat_val,
                                                                        cat_name))
        _cat_dict['value'][_dummy_name] = 1
        return _cat_dict['value']

    raise Exception('unknown category {}'.format(cat_name))


def is_keep_column(col_name):
    """
    Decides whether a given column is part of the features set
    """
    if col_name in keep_columns:
        return True
    else:
        return False


def get_min_max_scaling(value_to_be_scaled, cat_name):
    """
    finds mapping for the category and returns scaled result with cat_name
    """
    if cat_name not in min_max_mapping:
        raise Exception('unknown category {}'.format(cat_name))

    _min_max_values = min_max_mapping[cat_name]
    _output = min_max_scaling(value_to_be_scaled, _min_max_values["min_val"], _min_max_values["max_val"])
    return _output


if __name__ == '__main__':
    # _cat_val = "head of household"
    # _cat_name = "tax filer stat"
    #
    # print(map_dummies(_cat_name, _cat_val))
    # print(dummies_mapping.json[_cat_name]["value"])
    #
    #
    # _cat_val = "asian"
    # _cat_name = "race"
    #
    # print(map_dummies(_cat_name, _cat_val))
    # print(dummies_mapping.json[_cat_name]["value"])
    #
    #
    #
    # print(get_min_max_scaling(55, "wage per hour"))
    print(set(list(dummies_mapping.keys()) + list(min_max_mapping.keys())))
    print()