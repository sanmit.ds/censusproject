from scaling_dummies_mapping import get_min_max_scaling, map_dummies, is_keep_column, dummies_mapping
import pandas as pd
import json

"""
Functionality related to mapping input the the ML model and gathering output from the ML model
"""

def classify_payload(payload_input, model):
    """
    given a model return the classification of the payload passed
    """
    _dummies = dummies_and_scaling(payload_input)
    _model_input = pd.DataFrame.from_dict([_dummies])
    predictions = model.predict(_model_input)
    predictions = pd.DataFrame(predictions)
    predictions = predictions.replace(1, "50000+")
    predictions = predictions.replace(0, "-50000")
    predictions = predictions[0].tolist()

    if len(predictions) > 1:
        return predictions
    elif len(predictions) == 1:
        return predictions[0]
    else:
        raise Exception("Could not classify {}".format(json.dumps(payload_input)))


def dummies_and_scaling(payload_input):
    """
    find the dummy variables for categories
    scale the numeric variables for numerical values
    """
    _for_classification = {}
    for _key, _value in payload_input.items():
        if is_keep_column(_key):
            if _key in dummies_mapping:
                if type(_value) is not str:
                    _value = str(_value)
                _dummies = map_dummies(_key.lower(), _value.lower())
                _for_classification.update(_dummies)

            else:
                _for_classification[_key] = get_min_max_scaling(_value, _key)
    return _for_classification
